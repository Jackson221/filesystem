/*!
 * @file
 * @author Jackson McNeill
 *
 * A small FS abstraction to fill the gaps SDL leaves with its own FS abstraction.
 *
 * Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include <vector>
#include <string>
#include <exception>
#include <optional>
//#include <assimp/IOStream.hpp>
//#include <assimp/IOSystem.hpp>

#include "test/forward.hpp"

namespace filesystem
{
	extern std::string app_root_directory;
	extern std::string temp_directory;
	extern std::string config_directory;


	void init(std::string program_path);

	struct FileNotFound : std::exception
	{
		const char* msg;
		std::string filename;
		FileNotFound(std::string filename);
		const char* what() const noexcept;
	};
	struct NotConvertibleToDOS : std::exception
	{
		const char* msg;
		std::string filename;
		NotConvertibleToDOS(std::string filename);
		const char* what() const noexcept;
	};
	struct NotConvertibleToUNIXish : std::exception
	{
		const char* msg;
		std::string filename;
		NotConvertibleToUNIXish(std::string filename);
		const char* what() const noexcept;
	};

	/*!
	 * Converts a DOS filepath to a more UNIX-like one
	 * Ex: "C:\a\dos\path" -> "/C/a/dos/path"
	 */
	std::string DOS_to_UNIXish(std::string DOS_path);

	/*!
	 * Converts a UNIX-ish path generated by DOS_to_UNIXish to one that DOS-like systems understand
	 * Ex: "/C/a/dos/path" -> "C:\a\dos\path"
	 *
	 * Will throw NotConvertibleToDOS if a path is not possbile to represent in a DOS-like system
	 * Ex. "/" or a path is longer than 255 chars (which Windows cannot handle)
	 */
	std::string UNIXish_to_DOS(std::string UNIXISH_path);


	std::string canonicalize_path(std::string path);

	std::string host_os_path_format(std::string path);

	std::string generic_path_format(std::string path);

	/*!
	 * Gets the position that a file extension is at
	 */
	std::optional<size_t> get_filename_extension_position(std::string fullfilename);
	std::optional<size_t> get_filename_extension_position(const char* fullfilename);

	/*!
	 * Gets the position that the final / in path is at
	 */
	std::optional<size_t> get_parrent_directory_position(std::string fullfilename);
	std::optional<size_t> get_parrent_directory_position(const char* fullfilename);


	/*!
	 * Gets file extension. 
	 * Example: doc.txt -> txt
	 */
	std::optional<std::string> get_filename_extension(const char* fullfilename);
	std::optional<std::string> get_filename_extension(std::string fullfilename);

	/*!
	 * Strips the extension off a file
	 * Example: doc.txt -> doc
	 */
	std::string get_filename_without_extension(std::string fullfilename);
	std::string get_filename_without_extension(const char* fullfilename);

	/*!
	 * Gives you the enclosing directory
	 *
	 * Example: folder/folder2/file -> folder/folder2
	 */
	std::string get_parrent_directory(std::string fullfilename);
	std::string get_parrent_directory(const char* fullfilename);

	/*!
	 * Returns a std::string which has the contents of a file
	 */
	std::string read_all_from_file(std::string fullfilename);
	std::string read_all_from_file(const char* fullfilename);
	
	/*!
	 * Concatenates directories
	 *
	 * e.g. /home/pi,my/project -> /home/pi/my/project
	 */
	std::string concat_directories(std::string a, std::string b);

	/*!
	 * Append the application path (e.x. ~/module3D") to a relative path (ex. "shader.frag" --> "~/module3D/shader.frag")
	 */
	std::string absolute_path(std::string application_relative_path);

	template<typename ... T>
	std::string concat_directories(std::string a, std::string b, T ... c)
	{
		return concat_directories(concat_directories(a,b),c...);
	}


	/*!
	 * Overwrites to a file and closes it.
	 *
	 * Will create the file if it doesn't exist, or overwrite it if it does.
	 */
	void write_to_file(const char* fullfilename, const char* contents, size_t size);
	void write_to_file(const char* fullfilename, std::string contents);
	void write_to_file(std::string fullfilename, std::string contents);

	/*!
	 * Overwrites to a file and closes it.
	 *
	 * Will create the file if it doesn't exist, or overwrite it if it does.
	 */
	void append_to_file(const char* fullfilename, const char* contents, size_t size);
	void append_to_file(const char* fullfilename, std::string contents);
	void append_to_file(std::string fullfilename, std::string contents);

	/*!
	 * Returns the base file name
	 *
	 * Example: folder/folder2/fiLe.txt -> fiLe.txt
	 */
	std::string get_base_filename(std::string fullfilename);
	std::string get_base_filename(const char* fullfilename);

	void unit_test(test::instance& inst);
}
