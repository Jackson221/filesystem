//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "filesystem.hpp"

#include <iostream>
#include <cstdio>
#include <algorithm>
#include <regex>

#include <SDL.h>
//#include <assimp/IOStream.hpp>
//#include <assimp/IOSystem.hpp>
//#include "logger/logger.hpp"

#include "test/test.hpp"

namespace filesystem
{
	/*sdl_assimp_io_stream::sdl_assimp_io_stream( const char* pFile, const char* pMode)
	{
		rwops = SDL_RWFromFile(pFile,pMode);
		//TODO debug
		if (!Exists())
		{
			logger::console_log<not_exist_msg>(pFile);
		}
	}

	// Check whether a specific file exists
	bool sdl_io_system::Exists( const char* str) const
	{
		return true; //TODO
	}
	// Get the path delimiter character we'd like to see
	char sdl_io_system::getOsSeparator() const
	{
		return '/';
	}
	// ... and finally a method to open a custom stream
	Assimp::IOStream* sdl_io_system::Open( const char* pFile, const char* pMode) 
	{
		return new sdl_assimp_io_stream( pFile, pMode );
	}
	void sdl_io_system::Close( Assimp::IOStream* pFile) 
	{
		delete pFile; 
	}*/






	FileNotFound::FileNotFound(std::string filename) :
		filename(filename)
	{
		filename = ("File "+filename+" not found");
		msg = filename.c_str();
	}
	const char* FileNotFound::what() const noexcept { return msg; }
	NotConvertibleToDOS::NotConvertibleToDOS(std::string filename) : 
		filename(filename)
	{
		filename = ("Path "+filename+" is not convertible to DOS");
		msg = filename.c_str();
	}
	const char* NotConvertibleToDOS::what() const noexcept { return msg; }
	NotConvertibleToUNIXish::NotConvertibleToUNIXish(std::string filename) :
		filename(filename)
	{
		filename = ("Path "+filename+" is not convertible to a UNIXish path");
		msg = filename.c_str();
	}
	const char* NotConvertibleToUNIXish::what() const noexcept { return msg; }

	std::string UNIX_to_DOS_slashes(std::string victim) 
	{
		return std::regex_replace(victim, std::regex("/"), std::string("\\"));
	}
	std::string DOS_to_UNIX_slashes(std::string boon) 
	{
		return std::regex_replace(boon, std::regex("\\\\"), std::string("/"));
	}
	std::optional<size_t> get_first_slash_position(std::string path)
	{
		for (size_t i=0;i<=path.size();i++)
		{
			if (path[i] == '/')
			{
				return {i};
			}
		}
		return {};
	}
	std::string canonicalize_path(std::string path) //TODO more canonicalization
	{
		bool last_was_slash = false;
		for (size_t i=0;i<=path.size();i++)
		{
			if (path[i] == '/')
			{
				if (last_was_slash)
				{
					path = path.substr(0,i-1)+path.substr(i);
					return canonicalize_path(path);
				}
				last_was_slash = true;
			}
			else
			{
				last_was_slash=false;
			}
		}
		return path;
	}

	std::string host_os_path_format(std::string path)
	{
#ifdef PLATFORM_TYPE_DOSLIKE
		path = UNIXish_to_DOS(path);
#endif
		return path;
	}
	std::string generic_path_format(std::string path)
	{
#ifdef PLATFORM_TYPE_DOSLIKE
		path = DOS_to_UNIXish(path);
#endif
		return path;
	}
	std::string UNIXish_to_DOS(std::string UNIXISH_path)
	{
		UNIXISH_path = canonicalize_path(UNIXISH_path);
		if (UNIXISH_path[0] == '/')
		{
			UNIXISH_path = UNIXISH_path.substr(1);
			auto position = get_first_slash_position(UNIXISH_path);
			if (!position.has_value())
			{
				throw NotConvertibleToDOS(UNIXISH_path);
			}
			auto rest_of_path = UNIXISH_path.substr(position.value()+1);
			rest_of_path = UNIX_to_DOS_slashes(rest_of_path);
			return UNIXISH_path.substr(0,position.value())+":\\"+rest_of_path;
		}
		else 
		{
			return UNIX_to_DOS_slashes(UNIXISH_path);
		}
	}
	std::string DOS_to_UNIXish(std::string DOS_path)
	{
		std::string DOS_path_with_UNIX_slashes = DOS_to_UNIX_slashes(DOS_path);
		std::string out;
		//search for a drive lettering
		if(DOS_path_with_UNIX_slashes[1] == ':')
		{
			char drive_letter = DOS_path_with_UNIX_slashes[0];
			out = std::string("/")+drive_letter+"/";
			DOS_path_with_UNIX_slashes = DOS_path_with_UNIX_slashes.substr(2);
		}
		out = out + DOS_path_with_UNIX_slashes;
		out = canonicalize_path(out);
		return out;
	}
#include <iostream>

	std::string app_root_directory;
	std::string temp_directory = 
#ifdef PLATFORM_TYPE_POSIX
	"/tmp/";
#elif defined(PLATFORM_TYPE_DOSLIKE)
	getenv("TEMP");
#else
	"/tmp/";
#endif
std::string config_directory;



	void init(std::string program_path)
	{
#ifdef PLATFORM_TYPE_POSIX
		char* result = getenv("XDG_CONFIG_HOME");
		config_directory = result != nullptr? result : "";
#elif defined(PLATFORM_TYPE_DOSLIKE)
		config_directory = getenv("APPDATA");
#endif
		if (config_directory.empty())
		{
			config_directory = filesystem::concat_directories(getenv("HOME"),"/.config/");
		}


#ifdef PLATFORM_ANDROID
		app_root_directory = "";//filesystem::concat_directories(std::string(SDL_AndroidGetInternalStoragePath()), "/assets");
#else
#ifdef PLATFORM_TYPE_DOSLIKE
		printf("was %s\n",program_path.c_str());
		program_path = DOS_to_UNIXish(program_path);
		printf("now %s\n",program_path.c_str());
#endif
		app_root_directory = get_parrent_directory(get_parrent_directory(program_path));
#endif
	}
	std::optional<size_t> get_filename_extension_position(std::string fullfilename)
	{
		std::optional<size_t> position = {};
		for (size_t i=0;i<=fullfilename.size();i++)
		{
			if (fullfilename[i] == '.')
			{
				position= {i};
			}
		}
		return position;
	}
	std::optional<size_t> get_filename_extension_position(const char* fullfilename)
	{
		return get_filename_extension_position(std::string(fullfilename));
	}
	std::optional<size_t> get_parrent_directory_position(std::string fullfilename)
	{
		std::optional<size_t> position = {};
		for (size_t i=0;i<=fullfilename.size();i++)
		{
			if (fullfilename[i] == '/')
			{
				position= {i};
			}
		}
		return position;
	}
	std::optional<size_t> get_parrent_directory_position(const char* fullfilename)
	{
		return get_parrent_directory_position(std::string(fullfilename));
	}


	std::optional<std::string> get_filename_extension(std::string fullfilename)
	{
		auto position = get_filename_extension_position(fullfilename);
		if (!position.has_value())
		{
			return {};
		}
		return  fullfilename.substr(position.value()+1,fullfilename.size());
	}
	std::optional<std::string> get_filename_extension(const char* fullfilename)
	{
		return get_filename_extension(std::string(fullfilename));
	}


	std::string get_filename_without_extension(std::string fullfilename)
	{
		auto position = get_filename_extension_position(fullfilename);
		return (position.has_value())? fullfilename.substr(0,position.value()) : fullfilename;
	}
	std::string get_filename_without_extension(const char* fullfilename)
	{
		return get_filename_without_extension(std::string(fullfilename));
	}

	std::optional<size_t> get_last_ellipsis_position(std::string fullfilename)
	{
		for (size_t i=fullfilename.size()-1;i!=0;i--)
		{
			if (fullfilename[i] == '.' && fullfilename[i-1] == '.')
			{
				if (i-2 == size_t(-1) || fullfilename[i-2] == '/')
				{
					if (i+1 >= fullfilename.size() || fullfilename[i+1] == '/')
					{
						return {i-1};
					}
				}
			}
		}
		return {};
	}


	std::string get_parrent_directory(std::string fullfilename)
	{
		auto position = get_parrent_directory_position(fullfilename);
		if (position.has_value())
		{
			return fullfilename.substr(0,position.value());
		}
		auto ellipsis_pos = get_last_ellipsis_position(fullfilename);
		if (ellipsis_pos.has_value())
		{
			return concat_directories(fullfilename,"..");
		}
		return "..";
	}
	std::string get_parrent_directory(const char* fullfilename)
	{
		return get_parrent_directory(std::string(fullfilename));
	}

	std::string get_base_filename(std::string fullfilename)
	{
		auto position = get_parrent_directory_position(fullfilename);
		return position.has_value()?  fullfilename.substr(position.value()+1,fullfilename.size()) : fullfilename;
	}
	std::string get_base_filename(const char* fullfilename)
	{
		return get_base_filename(std::string(fullfilename));
	}

	std::string concat_directories(std::string a, std::string b)
	{
		if (a.empty())
		{
			return b;
		}
		else if (b.empty())
		{
			return a;
		}

		if (b[0] == '/')
		{
			b = b.substr(1);
		}
		if (a.back() == '/')
		{
			return a+b;
		}
		else
		{
			return a + "/" + b;
		}
	}

	std::string absolute_path(std::string application_relative_path)
	{
		return concat_directories(app_root_directory,application_relative_path);
	}


	const size_t bufferSize = 256;
	std::string read_all_from_file(const char* fullfilename)
	{
#ifdef PLATFORM_TYPE_DOSLIKE
		std::string fixed = UNIXish_to_DOS(std::string(fullfilename));
		fullfilename = fixed.c_str();
#endif
		char buffer[bufferSize] = "";
		std::string fullBuffer;
		SDL_RWops *file = SDL_RWFromFile(fullfilename, "rb");
		size_t objsRead;

		if (file == NULL) 
		{
			throw FileNotFound{fullfilename};
		}
		else
		{
			size_t fileSize = static_cast<size_t>(SDL_RWsize(file));
			size_t remainingBytes = fileSize;

			fullBuffer.reserve(fileSize);  //avoid expensive reallocs
			while (true)
			{	
				objsRead = SDL_RWread(file, buffer, sizeof (buffer), 1);//read as much as our buffer will store

				size_t charsToRead = std::min(remainingBytes,bufferSize); //if there are less bytes remaining to read than what the buffer can store, then only read that many (e.g. 121 instead of 256)

				for (size_t i=0;i!=charsToRead;i++)
				{
					fullBuffer.push_back(buffer[i]);	
				}
				
				if (objsRead == 0)  //last object wasn't full -- next would be empty
				{
					break; 
				}
				
				remainingBytes -= bufferSize; //we've read a full buffer
			}
			SDL_RWclose(file);
		}
		return fullBuffer;
	}
	std::string read_all_from_file(std::string fullfilename)
	{
		return read_all_from_file(fullfilename.c_str());
	}
	void write_to_file(const char* fullfilename, const char* contents, size_t size)
	{
#ifdef PLATFORM_TYPE_DOSLIKE
		std::string fixed = UNIXish_to_DOS(std::string(fullfilename));
		fullfilename = fixed.c_str();
		printf("PATH %s\n",fullfilename);
#endif
		SDL_RWops *file = SDL_RWFromFile(fullfilename, "w");
		SDL_RWwrite(file, contents,size,1);
		SDL_RWclose(file);
	}
	void write_to_file(const char* fullfilename, std::string contents)
	{
		write_to_file(fullfilename,contents.data(),contents.length());
	}
	void write_to_file(std::string fullfilename, std::string contents)
	{
		write_to_file(fullfilename.data(),contents);
	}
	void append_to_file(const char* fullfilename, const char* contents, size_t size)
	{
#ifdef PLATFORM_TYPE_DOSLIKE
		std::string fixed = UNIXish_to_DOS(std::string(fullfilename));
		fullfilename = fixed.c_str();
#endif
		SDL_RWops *file = SDL_RWFromFile(fullfilename, "a");
		SDL_RWwrite(file, contents,size,1);
		SDL_RWclose(file);
	}
	void append_to_file(const char* fullfilename, std::string contents)
	{
		append_to_file(fullfilename,contents.c_str(),contents.length());
	}
	void append_to_file(std::string fullfilename, std::string contents)
	{
		append_to_file(fullfilename.c_str(),contents);
	}






	void unit_test(test::instance& inst)//mostly tests string manipulation
	{
		inst.set_name("Filesystem");
		inst.test(TEST_0(get_filename_without_extension("my.file.ext"),"my.file"));
		inst.test(TEST_0(get_filename_extension("my.file.ext"),{"ext"}));
		inst.test(TEST_0(get_parrent_directory("folder/file"),"folder"));
		inst.test(TEST_0(get_base_filename("folder/file"),"file"));
		//concat dirs
		inst.test(TEST_0(concat_directories("folder/file",""),"folder/file"));
		inst.test(TEST_0(concat_directories("folder/file","thing"),"folder/file/thing"));
		inst.test(TEST_0(concat_directories("","folder/file"),"folder/file"));
		inst.test(TEST_0(concat_directories("",""),""));
		//post-double concat dirs
		inst.test(TEST_0(concat_directories("folder/file","thing","third","fourth"),"folder/file/thing/third/fourth"));

		inst.test(TEST_0(get_filename_without_extension("file"),"file"));
		inst.test(TEST_0(get_filename_extension("file"),{}));
		inst.test(TEST_0(get_parrent_directory("folder"),".."));
		inst.test(TEST_0(get_base_filename("file"),"file"));

		std::string teststr = "this IS a test write. Null(\0";
		auto test_file_loc = concat_directories(temp_directory,"test-file");
		write_to_file(test_file_loc,teststr);
		std::string returnStr = read_all_from_file(test_file_loc);

		inst.inform("Ensure file IO works");
		inst.test(TEST_2(teststr,returnStr, teststr==returnStr,true));
		inst.inform("Ensure null character works");
		inst.test(TEST_1(returnStr, returnStr[27] == '\0',true));

	}

}
